package com.elsennovraditya.chatapp.chat.presenter;

import com.elsennovraditya.chatapp.chat.interactor.ChatInteractor;
import com.elsennovraditya.chatapp.chat.interactor.ChatInteractorImpl;
import com.elsennovraditya.chatapp.chat.ui.ChatView;

/**
 * Created by elsen on 2/7/15.
 */
public class ChatPresenterFactory {

    public static ChatPresenter buildChatPresenter(ChatView chatView) {
        ChatInteractor chatInteractor = new ChatInteractorImpl();
        ChatPresenter chatPresenter = new ChatPresenterImpl(chatView, chatInteractor);
        return chatPresenter;
    }

}
