package com.elsennovraditya.chatapp.chat.listener;

import com.elsennovraditya.chatapp.chat.ui.ChatView;
import com.elsennovraditya.chatapp.universal.Constant;
import com.layer.sdk.LayerClient;
import com.layer.sdk.messaging.Conversation;

/**
 * Created by elsen on 2/9/15.
 */
public class LayerTypingIndicatorListenerImpl implements
        com.layer.sdk.listeners.LayerTypingIndicatorListener {

    private ChatView mChatView;

    public LayerTypingIndicatorListenerImpl(ChatView chatView) {
        mChatView = chatView;
    }

    @Override
    public void onTypingIndicator(LayerClient layerClient, Conversation conversation, String userId,
            TypingIndicator typingIndicator) {

        switch (typingIndicator) {
            case STARTED:
                if (userId.equals(Constant.PARTICIPANT_ID)) {
                    mChatView.onParticipantStartTyping();
                }
                break;
            case PAUSED:
            case FINISHED:
                if (userId.equals(Constant.PARTICIPANT_ID)) {
                    mChatView.onParticipantStopTyping();
                }
                break;
        }

    }
}
