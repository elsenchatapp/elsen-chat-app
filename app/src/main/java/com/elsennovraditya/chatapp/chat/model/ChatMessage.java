package com.elsennovraditya.chatapp.chat.model;

/**
 * Created by elsen on 2/6/15.
 */
public class ChatMessage {

    private boolean mIsMyMessage;

    private String mMessage;

    public ChatMessage(boolean isMyMessage, String message) {
        this.mIsMyMessage = isMyMessage;
        this.mMessage = message;
    }

    public boolean isMyMessage() {
        return mIsMyMessage;
    }

    public void setIsMyMessage(boolean isMyMessage) {
        mIsMyMessage = isMyMessage;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
