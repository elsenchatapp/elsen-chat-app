package com.elsennovraditya.chatapp.chat.listener;

import com.elsennovraditya.chatapp.chat.ui.ChatFragment;
import com.elsennovraditya.chatapp.chat.ui.ChatView;
import com.elsennovraditya.chatapp.universal.Constant;
import com.layer.sdk.LayerClient;
import com.layer.sdk.exceptions.LayerException;
import com.layer.sdk.listeners.LayerAuthenticationListener;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elsen on 2/7/15.
 */
public class LayerAuthenticationListenerImpl implements LayerAuthenticationListener {

    private static final String TAG = LayerAuthenticationListenerImpl.class.getSimpleName();

    private ChatView mChatView;

    public LayerAuthenticationListenerImpl(ChatView chatView) {
        mChatView = chatView;
    }

    @Override
    public void onAuthenticated(LayerClient layerClient, String s) {
        mChatView.onUserAuthenticated();
    }

    @Override
    public void onDeauthenticated(LayerClient layerClient) {
        Log.d(TAG, "onDeauthenticated");
    }

    @Override
    public void onAuthenticationChallenge(final LayerClient layerClient, final String nonce) {
        final String mUserId = Constant.CURRENT_USER_ID;
        ParseCloud.callFunctionInBackground("generateToken",
                createParamsForGeneratingToken(mUserId, nonce),
                new GenerateTokenCallback(layerClient));
    }

    private Map<String, String> createParamsForGeneratingToken(String userId, String nonce) {
        Map<String, String> params = new HashMap<>();
        params.put("userID", userId);
        params.put("nonce", nonce);
        return params;
    }

    @Override
    public void onAuthenticationError(LayerClient layerClient, LayerException e) {
        Log.d(TAG, e.getLocalizedMessage());
        Log.d(TAG, e.getMessage());
    }

    private static class GenerateTokenCallback extends FunctionCallback<String> {

        private final String TAG = this.getClass().getSimpleName();

        private LayerClient mLayerClient;

        GenerateTokenCallback(LayerClient layerClient) {
            mLayerClient = layerClient;
        }

        @Override
        public void done(String token, ParseException e) {
            if (e == null) {
                mLayerClient.answerAuthenticationChallenge(token);
                Log.d(TAG, "Parse Cloud generate token function success to be called");
            } else {
                Log.d(TAG, "Parse Cloud function failed to be called to generate token with error: "
                        + e.getMessage());
            }
        }
    }
}
