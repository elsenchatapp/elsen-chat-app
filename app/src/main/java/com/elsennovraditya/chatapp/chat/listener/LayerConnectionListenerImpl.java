package com.elsennovraditya.chatapp.chat.listener;

import com.elsennovraditya.chatapp.chat.ui.ChatView;
import com.layer.sdk.LayerClient;
import com.layer.sdk.exceptions.LayerException;
import com.layer.sdk.listeners.LayerConnectionListener;

import android.util.Log;

/**
 * Created by elsen on 2/7/15.
 */
public class LayerConnectionListenerImpl implements LayerConnectionListener {

    private static final String TAG = LayerConnectionListenerImpl.class.getSimpleName();

    private ChatView mChatView;

    public LayerConnectionListenerImpl(ChatView chatView) {
        mChatView = chatView;
    }

    @Override
    public void onConnectionConnected(LayerClient layerClient) {
        if (layerClient.isAuthenticated()) {
            mChatView.onUserAuthenticated();
        } else {
            layerClient.authenticate();
        }
    }

    @Override
    public void onConnectionDisconnected(LayerClient layerClient) {
        Log.d(TAG, "onConnectionDisconnected");
    }

    @Override
    public void onConnectionError(LayerClient layerClient, LayerException e) {
        Log.d(TAG, e.getLocalizedMessage());
        Log.d(TAG, e.getMessage());
    }
}
