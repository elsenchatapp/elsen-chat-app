package com.elsennovraditya.chatapp.chat.listener;

import com.elsennovraditya.chatapp.R;
import com.elsennovraditya.chatapp.universal.ui.ChatAppActivity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by elsen on 2/7/15.
 */
public class LayerPushReceiver extends BroadcastReceiver {

    private static final String TAG = LayerPushReceiver.class.getSimpleName();

    public static final String LAYER_PUSH_MESSAGE = "layer-push-message";

    public static final String LAYER_CONVERSATION_ID = "layer-conversation-id";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive LayerPushReceiver");

        //Don't show a notification on boot
        if (intent.getAction() == Intent.ACTION_BOOT_COMPLETED) {
            return;
        }

        // Get notification content
        Bundle extras = intent.getExtras();
        String message = "";
        Uri conversationId = null;
        if (extras.containsKey(LAYER_PUSH_MESSAGE)) {
            message = extras.getString(LAYER_PUSH_MESSAGE);
        }
        if (extras.containsKey(LAYER_CONVERSATION_ID)) {
            conversationId = extras.getParcelable(LAYER_CONVERSATION_ID);
        }

        // Build the notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setLights(context.getResources().getColor(R.color.send_button_default), 100, 1900)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE);

        // Set the action to take when a user taps the notification
        Intent resultIntent = new Intent(context, ChatAppActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.putExtra(LAYER_CONVERSATION_ID, conversationId);
        PendingIntent resultPendingIntent = PendingIntent
                .getActivity(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        // Show the notification
        NotificationManager mNotifyMgr = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.notify(1, mBuilder.build());
    }
}
