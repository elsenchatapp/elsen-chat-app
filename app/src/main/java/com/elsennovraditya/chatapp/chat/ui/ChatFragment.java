package com.elsennovraditya.chatapp.chat.ui;

import com.elsennovraditya.chatapp.ChatAppApplication;
import com.elsennovraditya.chatapp.R;
import com.elsennovraditya.chatapp.chat.listener.LayerAuthenticationListenerImpl;
import com.elsennovraditya.chatapp.chat.listener.LayerChangeEventListenerImpl;
import com.elsennovraditya.chatapp.chat.listener.LayerConnectionListenerImpl;
import com.elsennovraditya.chatapp.chat.listener.LayerPushReceiver;
import com.elsennovraditya.chatapp.chat.listener.LayerTypingIndicatorListenerImpl;
import com.elsennovraditya.chatapp.chat.model.ChatMessage;
import com.elsennovraditya.chatapp.chat.presenter.ChatPresenter;
import com.elsennovraditya.chatapp.chat.presenter.ChatPresenterFactory;
import com.elsennovraditya.chatapp.universal.Constant;
import com.elsennovraditya.chatapp.universal.ui.ChatAppActivity;
import com.layer.sdk.LayerClient;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.listeners.LayerTypingIndicatorListener;
import com.layer.sdk.messaging.LayerObject;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by elsen on 2/6/15.
 */
public class ChatFragment extends Fragment implements ChatView {

    private static final String TAG = ChatFragment.class.getSimpleName();

    public static final String PARTICIPANT_ID_KEY = "Participant ID Key";

    private static final long ONE_SECOND_IN_MILLIS = 1000;

    @InjectView(R.id.loading_layout)
    LinearLayout loadingLayout;

    @InjectView(R.id.chat_main_content)
    RelativeLayout chatMainContent;

    @InjectView(R.id.chat_container)
    ListView chatContainer;

    @InjectView(R.id.typed_message)
    EditText typedMessage;

    @InjectView(R.id.send_button)
    Button sendButton;

    private TextView mTypingIndicator;

    private TextView mParticipantName;

    private ActionBar mActionBar;

    private String mParticipantID;

    private Uri mConversationID;

    private ChatPresenter mChatPresenter;

    private ChatAppActivity mChatAppActivity;

    private ChatAdapter mChatAdapter;

    private LayerConnectionListenerImpl mLayerConnectionListener;

    private LayerAuthenticationListenerImpl mLayerAuthenticationListener;

    private LayerChangeEventListenerImpl mLayerChangeEventListener;

    private LayerTypingIndicatorListenerImpl mLayerTypingIndicatorListenerImpl;

    private CountDownTimer mCountDownTimer;

    public ChatFragment() {
        mChatPresenter = ChatPresenterFactory.buildChatPresenter(this);
        mLayerConnectionListener = new LayerConnectionListenerImpl(this);
        mLayerAuthenticationListener = new LayerAuthenticationListenerImpl(this);
        mLayerChangeEventListener = new LayerChangeEventListenerImpl(this);
        mLayerTypingIndicatorListenerImpl = new LayerTypingIndicatorListenerImpl(this);
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        mChatAppActivity = (ChatAppActivity) activity;
        getLayerClient().registerConnectionListener(mLayerConnectionListener);
        getLayerClient().registerAuthenticationListener(mLayerAuthenticationListener);
        getLayerClient().registerEventListener(mLayerChangeEventListener);
        getLayerClient().registerTypingIndicator(mLayerTypingIndicatorListenerImpl);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        getLayerClient().unregisterConnectionListener(mLayerConnectionListener);
        getLayerClient().unregisterAuthenticationListener(mLayerAuthenticationListener);
        getLayerClient().unregisterEventListener(mLayerChangeEventListener);
        getLayerClient().unregisterTypingIndicator(mLayerTypingIndicatorListenerImpl);
        mChatAppActivity = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.chat_fragment, container, false);
        ButterKnife.inject(this, rootView);
        initParticipantId();
        initConversationID();
        initCustomActionBar();
        initTypedMessage();
        initSendButton();
        initCountDownTimer();
        checkIfHasBeenConnectedAndAuthenticated();
        return rootView;
    }

    private void initCustomActionBar() {
        mActionBar = mChatAppActivity.getSupportActionBar();
        mActionBar.setCustomView(R.layout.custom_action_bar);

        mParticipantName = (TextView) mActionBar.getCustomView()
                .findViewById(R.id.participant_name);
        mParticipantName.setText(mParticipantID);
        mTypingIndicator = (TextView) mActionBar.getCustomView()
                .findViewById(R.id.typing_indicator);

        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
    }

    private void initParticipantId() {
        mParticipantID = getArguments().getString(PARTICIPANT_ID_KEY, "");
    }

    private void initConversationID() {
        mConversationID = getArguments().getParcelable(LayerPushReceiver.LAYER_CONVERSATION_ID);
    }

    private void initAdapterForChatContainer() {
        List<ChatMessage> chatMessageList = mChatPresenter
                .createChatMessageList(mParticipantID, mConversationID);
        mChatAdapter = new ChatAdapter(mChatAppActivity,
                R.layout.chat_fragment_single_message,
                chatMessageList);
        chatContainer.setAdapter(mChatAdapter);
        scrollToBottomOfChatContainer();
    }

    private void initTypedMessage() {
        typedMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setBackground();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setBackground();
                mChatPresenter
                        .sendTypingIndicator(LayerTypingIndicatorListener.TypingIndicator.STARTED);
                mCountDownTimer.cancel();
                mCountDownTimer.start();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            private void setBackground() {
                if (TextUtils.isEmpty(typedMessage.getText().toString())) {
                    sendButton.setEnabled(false);
                } else {
                    sendButton.setEnabled(true);
                }
            }
        });
    }

    private void initSendButton() {
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mChatPresenter.onClickSendButton(typedMessage.getText().toString());
            }
        });
    }

    private void initCountDownTimer() {
        mCountDownTimer = new CountDownTimer(ONE_SECOND_IN_MILLIS, ONE_SECOND_IN_MILLIS) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                mChatPresenter
                        .sendTypingIndicator(LayerTypingIndicatorListener.TypingIndicator.FINISHED);
            }
        };
    }

    private void checkIfHasBeenConnectedAndAuthenticated() {
        mChatPresenter.checkIfHasBeenConnectedAndAuthenticated();
    }

    @Override
    public void addMessageToChatContainer(ChatMessage chatMessage, boolean fromSendButton) {
        mChatAdapter.add(chatMessage);
        mChatAdapter.notifyDataSetChanged();
        clearTypedMessage(fromSendButton);
        scrollToBottomOfChatContainer();
    }

    private void clearTypedMessage(boolean fromSendButton) {
        if (fromSendButton) {
            typedMessage.setText("");
        }
    }

    private void scrollToBottomOfChatContainer() {
        if (mChatAdapter.getCount() > 0) {
            chatContainer.setSelection(mChatAdapter.getCount() - 1);
        }
    }

    @Override
    public LayerClient getLayerClient() {
        return ((ChatAppApplication) mChatAppActivity.getApplication()).getLayerClient();
    }

    @Override
    public String getCurrentUserID() {
        // For the future, get it from shared preference
        return Constant.CURRENT_USER_ID;
    }

    @Override
    public String getParticipantID() {
        return mParticipantID;
    }

    @Override
    public void onConversationChange(LayerObject layerObject,
            LayerChange.Type changeType) {
        mChatPresenter.onConversationChange(layerObject, changeType);
    }

    @Override
    public void onMessageChange(LayerObject layerObject, LayerChange.Type changeType) {
        mChatPresenter.onMessageChange(layerObject, changeType);
    }

    @Override
    public void onParticipantStartTyping() {
        mTypingIndicator.setVisibility(View.VISIBLE);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
    }

    @Override
    public void onParticipantStopTyping() {
        mTypingIndicator.setVisibility(View.GONE);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
    }

    @Override
    public void onUserAuthenticated() {
        if (isAdded()) {
            initAdapterForChatContainer();
            loadingLayout.setVisibility(View.GONE);
            chatMainContent.setVisibility(View.VISIBLE);
        }
    }

}
