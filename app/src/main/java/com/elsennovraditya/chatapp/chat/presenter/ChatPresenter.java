package com.elsennovraditya.chatapp.chat.presenter;

import com.elsennovraditya.chatapp.chat.model.ChatMessage;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.listeners.LayerTypingIndicatorListener;
import com.layer.sdk.messaging.LayerObject;

import android.net.Uri;

import java.util.List;

/**
 * Created by elsen on 2/7/15.
 */
public interface ChatPresenter {

    List<ChatMessage> createChatMessageList(String participantID, Uri uri);

    void onClickSendButton(String message);

    void onConversationChange(LayerObject layerObject, LayerChange.Type changeType);

    void onMessageChange(LayerObject layerObject, LayerChange.Type changeType);

    void sendTypingIndicator(LayerTypingIndicatorListener.TypingIndicator typingIndicator);

    void checkIfHasBeenConnectedAndAuthenticated();
}
