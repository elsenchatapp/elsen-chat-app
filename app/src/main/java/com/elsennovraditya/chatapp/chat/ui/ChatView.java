package com.elsennovraditya.chatapp.chat.ui;

import com.elsennovraditya.chatapp.chat.model.ChatMessage;
import com.layer.sdk.LayerClient;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.messaging.LayerObject;

/**
 * Created by elsen on 2/7/15.
 */
public interface ChatView {

    void addMessageToChatContainer(ChatMessage chatMessage, boolean fromSendButton);

    LayerClient getLayerClient();

    String getCurrentUserID();

    String getParticipantID();

    void onConversationChange(LayerObject layerObject,
            LayerChange.Type changeType);

    void onMessageChange(LayerObject layerObject, LayerChange.Type changeType);

    void onParticipantStartTyping();

    void onParticipantStopTyping();

    void onUserAuthenticated();
}
