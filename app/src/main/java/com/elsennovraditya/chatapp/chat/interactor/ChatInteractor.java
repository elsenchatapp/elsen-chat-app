package com.elsennovraditya.chatapp.chat.interactor;

import com.layer.sdk.LayerClient;

/**
 * Created by elsen on 2/7/15.
 */
public interface ChatInteractor {

    void sendMessageToParticipant(String chatMessage, LayerClient layerClient,
            String currentUserID, String participantID);

}
