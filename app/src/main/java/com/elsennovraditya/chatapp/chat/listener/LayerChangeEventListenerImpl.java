package com.elsennovraditya.chatapp.chat.listener;

import com.elsennovraditya.chatapp.chat.ui.ChatFragment;
import com.elsennovraditya.chatapp.chat.ui.ChatView;
import com.elsennovraditya.chatapp.universal.receiver.LayerChangeEventBridgeManager;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.changes.LayerChangeEvent;
import com.layer.sdk.listeners.LayerChangeEventListener;
import com.layer.sdk.messaging.LayerObject;

import java.util.List;

/**
 * Created by elsen on 2/7/15.
 */
public class LayerChangeEventListenerImpl implements LayerChangeEventListener {

    private ChatView mChatView;

    public LayerChangeEventListenerImpl(ChatView chatView) {
        mChatView = chatView;
    }

    public void onEventMainThread(LayerChangeEvent event) {
        List<LayerChange> changes = event.getChanges();
        for (LayerChange layerChange : changes) {
            LayerObject.Type objectType = layerChange.getObjectType();
            switch (objectType) {
                case CONVERSATION:
                    // Object is a conversation
                    doForConversationChanged(layerChange, objectType);
                    break;

                case MESSAGE:
                    // Object is a message
                    doForMessageChange(layerChange, objectType);
                    break;
            }
            break;
        }
    }

    private void doForConversationChanged(LayerChange layerChange, LayerObject.Type objectType) {
        LayerChange.Type changeType = layerChange.getChangeType();
        switch (changeType) {
            case INSERT:
                // Object was created
                mChatView.onConversationChange(layerChange.getObject(), changeType);
                break;

            case UPDATE:
                // Object was updated
                mChatView.onConversationChange(layerChange.getObject(), changeType);
                break;

            case DELETE:
                // Object was deleted
                mChatView.onConversationChange(layerChange.getObject(), changeType);
                break;
        }
    }

    private void doForMessageChange(LayerChange layerChange, LayerObject.Type objectType) {
        LayerChange.Type changeType = layerChange.getChangeType();
        switch (changeType) {
            case INSERT:
                // Object was created
                mChatView.onMessageChange(layerChange.getObject(), changeType);
                break;

            case UPDATE:
                // Object was updated
                mChatView.onMessageChange(layerChange.getObject(), changeType);
                break;

            case DELETE:
                // Object was deleted
                mChatView.onMessageChange(layerChange.getObject(), changeType);
                break;
        }
    }

}
