package com.elsennovraditya.chatapp.chat.presenter;

import com.elsennovraditya.chatapp.chat.interactor.ChatInteractor;
import com.elsennovraditya.chatapp.chat.model.ChatMessage;
import com.elsennovraditya.chatapp.chat.ui.ChatView;
import com.elsennovraditya.chatapp.universal.Constant;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.listeners.LayerTypingIndicatorListener;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.LayerObject;
import com.layer.sdk.messaging.Message;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by elsen on 2/6/15.
 */
public class ChatPresenterImpl implements ChatPresenter {

    private static final String TAG = ChatPresenterImpl.class.getSimpleName();

    private ChatView mChatView;

    private ChatInteractor mChatInteractor;

    private Conversation mConversation;

    public ChatPresenterImpl(ChatView chatView, ChatInteractor chatInteractor) {
        mChatView = chatView;
        mChatInteractor = chatInteractor;
    }

    @Override
    public List<ChatMessage> createChatMessageList(String participantID, Uri uri) {
        mConversation = getConversation(participantID, uri);

        List<Message> messages = null;
        if (mConversation != null) {
            messages = mChatView.getLayerClient().getMessages(mConversation);
        }

        List<ChatMessage> chatMessageList;
        if (messages != null) {
            chatMessageList = getAllMessages(messages);
        } else {
            chatMessageList = new ArrayList<>();
        }

        return chatMessageList;
    }

    private Conversation getConversation(String participantID, Uri uri) {
        List<Conversation> conversations;
        Conversation conversation = null;
        if (TextUtils.isEmpty(participantID)) {
            conversation = mChatView.getLayerClient().getConversation(uri);
        } else {
            conversations = mChatView.getLayerClient()
                    .getConversationsWithParticipants(participantID);
            if (conversations.size() > 0) {
                conversation = conversations.get(0);
            }
        }
        return conversation;
    }

    private List<ChatMessage> getAllMessages(List<Message> messages) {
        List<ChatMessage> chatMessageList = new ArrayList<>();
        for (Message message : messages) {
            ChatMessage chatMessage;
            if (message.getSentByUserId().equals(Constant.CURRENT_USER_ID)) {
                chatMessage = new ChatMessage(true, getMessage(message));
            } else {
                chatMessage = new ChatMessage(false, getMessage(message));
            }
            chatMessageList.add(chatMessage);
        }
        return chatMessageList;
    }

    @Override
    public void onClickSendButton(String message) {
        ChatMessage chatMessage = new ChatMessage(true, message);
        mChatView.addMessageToChatContainer(chatMessage, true);
        mChatInteractor
                .sendMessageToParticipant(message, mChatView.getLayerClient(),
                        mChatView.getCurrentUserID(), mChatView.getParticipantID());
    }

    @Override
    public void onConversationChange(LayerObject layerObject,
            LayerChange.Type changeType) {

    }

    @Override
    public void onMessageChange(LayerObject layerObject, LayerChange.Type changeType) {
        if (changeType.equals(LayerChange.Type.INSERT)) {
            Message message = (Message) layerObject;
            if (isTheMessageNotFromCurrentUser(message)) {
                mChatView.addMessageToChatContainer(new ChatMessage(false, getMessage(message)),
                        false);
            }
        } else if (changeType.equals(LayerChange.Type.UPDATE)) {
            Message message = (Message) layerObject;
            if (isTheMessageNotFromCurrentUser(message)) {
                mChatView.addMessageToChatContainer(new ChatMessage(false, getMessage(message)),
                        false);
            }
        }
    }

    @Override
    public void sendTypingIndicator(LayerTypingIndicatorListener.TypingIndicator typingIndicator) {
        mChatView.getLayerClient().sendTypingIndicator(mConversation, typingIndicator);
    }

    @Override
    public void checkIfHasBeenConnectedAndAuthenticated() {
        if (!mChatView.getLayerClient().isConnected()) {
            // Asks the LayerSDK to establish a network connection with the Layer service
            Log.d(TAG, "not connected yet");
            mChatView.getLayerClient().connect();
        } else if (!mChatView.getLayerClient().isAuthenticated()) {
            Log.d(TAG, "not authenticated yet");
            mChatView.getLayerClient().authenticate();
        } else {
            Log.d(TAG, "already connected and authenticated");
            mChatView.onUserAuthenticated();
        }
    }

    private boolean isTheMessageNotFromCurrentUser(Message message) {
        return !message.getSentByUserId().equals(Constant.CURRENT_USER_ID);
    }

    private String getMessage(Message message) {
        String chatMessage = null;
        try {
            chatMessage = new String(message.getMessageParts().get(0).getData(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return chatMessage;
    }
}
