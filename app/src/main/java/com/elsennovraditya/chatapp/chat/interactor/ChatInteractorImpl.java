package com.elsennovraditya.chatapp.chat.interactor;

import com.layer.sdk.LayerClient;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.Message;
import com.layer.sdk.messaging.MessagePart;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by elsen on 2/7/15.
 */
public class ChatInteractorImpl implements ChatInteractor {

    @Override
    public void sendMessageToParticipant(String chatMessage, LayerClient layerClient,
            String currentUserID, String participantID) {
        List<Conversation> allConversations = layerClient
                .getConversationsWithParticipants(currentUserID, participantID);

        Conversation conversation;
        if (allConversations.size() == 0) {
            // Creates and returns a new conversation object with sample participant identifiers
            conversation = Conversation.newInstance(Arrays.asList(participantID));
        } else {
            //Use an existing conversation
            conversation = allConversations.get(0);
        }

        // Create a message part with a string of text
        MessagePart messagePart = MessagePart.newInstance("text/plain", chatMessage.getBytes());

        // Creates and returns a new message object with the given conversation and array of message parts
        Message message = Message.newInstance(conversation, Arrays.asList(messagePart));

        // Add push notification
        Map<String, String> metadata = new HashMap<String, String>();
        metadata.put("layer-push-message", chatMessage);
        layerClient.setMetadata(message, metadata);

        //Sends the specified message
        layerClient.sendMessage(message);
    }

}
