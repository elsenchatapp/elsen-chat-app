package com.elsennovraditya.chatapp.chat.ui;

import com.elsennovraditya.chatapp.R;
import com.elsennovraditya.chatapp.chat.model.ChatMessage;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by elsen on 2/6/15.
 */
public class ChatAdapter extends ArrayAdapter<ChatMessage> {

    private LayoutInflater mLayoutInflater;

    private List<ChatMessage> mChatMessageList;

    public ChatAdapter(Context context, int resource,
            List<ChatMessage> objects) {
        super(context, resource, objects);
        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mChatMessageList = objects;
    }

    @Override
    public int getCount() {
        return mChatMessageList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.chat_fragment_single_message, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ChatMessage chatMessage = mChatMessageList.get(position);
        holder.message.setText(chatMessage.getMessage());
        preparePositionAndColorOfChatMessage(convertView, chatMessage);

        return convertView;
    }

    private void preparePositionAndColorOfChatMessage(View convertView, ChatMessage chatMessage) {
        RelativeLayout singleMessageConteiner = (RelativeLayout) convertView.findViewById(
                R.id.single_message_container);
        RelativeLayout singleMessageWrapper = (RelativeLayout) convertView
                .findViewById(R.id.single_message_wrapper);
        if (chatMessage.isMyMessage()) {
            singleMessageConteiner.setGravity(Gravity.RIGHT);
            singleMessageWrapper.setBackgroundResource(R.drawable.speech_bubble_right);
        } else {
            singleMessageConteiner.setGravity(Gravity.LEFT);
            singleMessageWrapper.setBackgroundResource(R.drawable.speech_bubble_left);
        }
    }

    static class ViewHolder {

        @InjectView(R.id.single_message)
        TextView message;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}
