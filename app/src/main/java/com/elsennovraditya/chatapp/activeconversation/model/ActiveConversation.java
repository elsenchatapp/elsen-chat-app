package com.elsennovraditya.chatapp.activeconversation.model;

/**
 * Created by elsen on 2/6/15.
 */
public class ActiveConversation {

    private String mLastMessage;

    private String mInteractedUser;

    public ActiveConversation(String lastMessage, String interactedUser) {
        mLastMessage = lastMessage;
        mInteractedUser = interactedUser;
    }

    public String getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(String lastMessage) {
        mLastMessage = lastMessage;
    }

    public String getInteractedUser() {
        return mInteractedUser;
    }

    public void setInteractedUser(String interactedUser) {
        mInteractedUser = interactedUser;
    }

}
