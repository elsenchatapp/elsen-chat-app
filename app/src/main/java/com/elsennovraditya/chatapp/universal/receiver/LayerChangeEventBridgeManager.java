package com.elsennovraditya.chatapp.universal.receiver;

import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.messaging.LayerObject;

import java.util.ArrayList;

/**
 * Created by elsen on 2/7/15.
 */
public class LayerChangeEventBridgeManager {

    private static final String TAG = LayerChangeEventBridgeManager.class.getSimpleName();

    private static volatile LayerChangeEventBridgeManager instance;

    private LayerChangeEventBridgeManager() {

    }

    public static LayerChangeEventBridgeManager getInstance() {
        if (instance == null) {
            synchronized (LayerChangeEventBridgeManager.class) {
                if (instance == null) {
                    final LayerChangeEventBridgeManager tmp = new LayerChangeEventBridgeManager();
                    instance = tmp;
                }
            }
        }
        return instance;
    }

    private final ArrayList<LayerChangeEventObserver> observers = new ArrayList<>();

    public void register(LayerChangeEventObserver observer) {
        observers.add(observer);
    }

    public void unregister(LayerChangeEventObserver observer) {
        observers.remove(observer);
    }

    public void notifyClient(LayerObject layerObject, LayerObject.Type objectType,
            LayerChange.Type changeType) {
        for (LayerChangeEventObserver observer : observers) {
            if (objectType.equals(LayerObject.Type.CONVERSATION)) {
                observer.onConversationChange(layerObject, changeType);
            } else {
                observer.onMessageChange(layerObject, changeType);
            }
        }
    }

}
