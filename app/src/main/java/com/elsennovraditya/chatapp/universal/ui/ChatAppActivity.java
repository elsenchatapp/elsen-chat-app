package com.elsennovraditya.chatapp.universal.ui;

import com.elsennovraditya.chatapp.R;
import com.elsennovraditya.chatapp.chat.listener.LayerPushReceiver;
import com.elsennovraditya.chatapp.chat.ui.ChatFragment;
import com.elsennovraditya.chatapp.universal.Constant;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class ChatAppActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_app_activity);
        if (savedInstanceState == null) {
            showChatConversation();
        }
    }

    private void showChatConversation() {
        if (getIntent().getParcelableExtra(LayerPushReceiver.LAYER_CONVERSATION_ID) == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, createChatFragmentFromParticipant())
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, createChatFragmentFromNotification())
                    .commit();
        }
    }

    private ChatFragment createChatFragmentFromParticipant() {
        ChatFragment chatFragment = new ChatFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ChatFragment.PARTICIPANT_ID_KEY, Constant.PARTICIPANT_ID);
        chatFragment.setArguments(bundle);
        return chatFragment;
    }

    private ChatFragment createChatFragmentFromNotification() {
        ChatFragment chatFragment = new ChatFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(LayerPushReceiver.LAYER_CONVERSATION_ID,
                getIntent().getParcelableExtra(LayerPushReceiver.LAYER_CONVERSATION_ID));
        chatFragment.setArguments(bundle);
        return chatFragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
