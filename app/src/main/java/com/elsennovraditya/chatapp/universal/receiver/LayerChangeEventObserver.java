package com.elsennovraditya.chatapp.universal.receiver;

import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.messaging.LayerObject;

/**
 * Created by elsen on 2/7/15.
 */
public interface LayerChangeEventObserver {

    void onConversationChange(LayerObject layerObject, LayerChange.Type changeType);

    void onMessageChange(LayerObject layerObject, LayerChange.Type changeType);

}
