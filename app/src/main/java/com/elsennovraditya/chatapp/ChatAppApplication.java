package com.elsennovraditya.chatapp;

import com.elsennovraditya.chatapp.chat.listener.LayerAuthenticationListenerImpl;
import com.elsennovraditya.chatapp.chat.listener.LayerChangeEventListenerImpl;
import com.elsennovraditya.chatapp.chat.listener.LayerConnectionListenerImpl;
import com.layer.sdk.LayerClient;
import com.parse.Parse;

import android.app.Application;

import java.util.UUID;

/**
 * Created by elsen on 2/7/15.
 */
public class ChatAppApplication extends Application {

    private static final String PARSE_APP_ID = "gnmrW6Av5qHMVWosWcWiXjM77lPrvmX53tJ1eohs";

    private static final String CLIENT_KEY = "i8gkxDsSSUG2D3MmYdNAIlTEI8rc9DvptGibWmrj";

    private static final String LAYER_APP_ID = "e4ea9696-8105-11e4-bd52-fcf374056717";

    private static final String GCM_SENDER_ID = "936446588437";

    private LayerClient mLayerClient;

    public LayerClient getLayerClient() {
        return mLayerClient;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initLayer();
        initParse();
    }

    private void initParse() {
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, PARSE_APP_ID, CLIENT_KEY);
    }

    private void initLayer() {
        UUID appID = UUID.fromString(LAYER_APP_ID);
        mLayerClient = LayerClient.newInstance(this, appID, GCM_SENDER_ID);
        mLayerClient.connect();
    }
}
